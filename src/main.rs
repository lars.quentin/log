use std::error::Error;
use std::fs::File;
use std::io::prelude::Write;

use plotlib::page::Page;
use plotlib::repr::Plot;
use plotlib::style::{LineStyle, LineJoin, PointMarker, PointStyle};
use plotlib::view::ContinuousView;

fn get_xs() -> Vec<u64> {
    vec![2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216,33554432,67108864,134217728]
}

fn get_ys() -> Vec<f64> {
    vec![0.00008932071511107086,0.0000047792594908834684,0.00012213719432931878,0.000004061171011520849,0.0001309916928711705,0.00011344915798020597,0.00011510105305292327,0.00012190507141972481,0.00013009892380135197,0.000004864117078565348,0.0000016180632892362484,0.0000017593763958377508,0.00018944491989934952,0.000009291753569820534,0.000009216611251128043,0.000016909784572373957,0.0005359701963595838,0.0008173141311682788,0.001555297384467223,0.0026838510892729346,0.006969143935949282,0.011052284088126306,0.018836571642956008,0.033348215725570424,0.06316671041993883,0.12123421860028796,0.23690994829685746]
}

fn interpolated_func(x: f64) -> f64 {
    1.77432023e-09 * x + 0.00081052
}

fn main() -> Result<(), Box<dyn Error>>{
    let xs = get_xs();
    let ys = get_ys();
    let mut xs_ys: Vec<(f64, f64)> = Vec::new();
    for i in 0..xs.len() {
        xs_ys.push((xs[i] as f64, ys[i]));
    }

    let pts = Plot::new(xs_ys).point_style(
        PointStyle::new()
        .colour("#ff0000")
        .marker(PointMarker::Cross)
    );
    let v = ContinuousView::new().add(pts);
    let svg_str = Page::single(&v).to_svg().unwrap().to_string();

    let mut file = File::create("./test.svg")?;
    write!(file, "{}", svg_str)?;

    Ok(())
}
